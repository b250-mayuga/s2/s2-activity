package com.zuitt.example;

import java.util.Scanner;
public class LeapYear {

    public static void main(String[] args){
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year.");

        int year = myScanner.nextInt();
        boolean leapYear = false;

        if(year % 4 == 0) {
            if(year % 100 ==0){
                if(year % 400 == 0)
                    leapYear = true;
                else leapYear = false;
            }
            else
                leapYear = true;
        }
        else
            leapYear = false;

        if(leapYear)
            System.out.println(year + " is a leap year.");
        else
            System.out.println(year + " is NOT a leap year.");

    }
}
