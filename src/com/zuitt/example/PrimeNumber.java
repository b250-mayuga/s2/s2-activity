package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
public class PrimeNumber {

    public static void main(String[] args){
        Scanner myScanner = new Scanner(System.in);

        int[] primeNumbers = { 2, 3, 5, 7, 11};

        System.out.println("Enter index of a prime number: ");
        int number = myScanner.nextInt();

        switch (number){
            case 1:
                System.out.println("The first prime number is: " + primeNumbers[number-1]);
                break;
            case 2:
                System.out.println("The second prime number is: " + primeNumbers[number-1]);
                break;
            case 3:
                System.out.println("The third prime number is: " + primeNumbers[number-1]);
                break;
            case 4:
                System.out.println("The fourth prime number is: " + primeNumbers[number-1]);
                break;
            case 5:
                System.out.println("The fifth prime number is: " + primeNumbers[number-1]);
                break;
            default:
                System.out.println("Invalid input. Please select from 1-5.");
        }

        ArrayList<String> myFriends = new ArrayList<String>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));

        System.out.println("My friends are " + myFriends);


        HashMap<String, Integer> contents = new HashMap<String, Integer>(){
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory consists of: " + contents);

    }
}
